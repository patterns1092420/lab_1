﻿using System;
using System.Collections.Generic;

interface IAnimal
{
    string Name { get; set; }
    void Feed();
}

// class for Lion
class Lion : IAnimal
{
    public string Name { get; set; }
    public void Feed()
    {
        Console.WriteLine($"{Name} the Lion is being fed.");
    }
}

// class for Tiger
class Tiger : IAnimal
{
    public string Name { get; set; }
    public void Feed()
    {
        Console.WriteLine($"{Name} the Tiger is being fed.");
    }
}

// Abstract class for enclosure
abstract class Enclosure
{
    public string Type { get; set; }
    public int Size { get; set; }
}

// Concrete class for specific type of enclosure
class Cage : Enclosure
{
    public Cage(int size)
    {
        Type = "Cage";
        Size = size;
    }
}

// Interface for employees
interface IEmployee
{
    string Name { get; set; }
    void Work();
}

// Concrete class for zookeeper
class Zookeeper : IEmployee
{
    public string Name { get; set; }
    public void Work()
    {
        Console.WriteLine($"{Name} is taking care of the animals.");
    }
}

// Concrete class for veterinarian
class Veterinarian : IEmployee
{
    public string Name { get; set; }
    public void Work()
    {
        Console.WriteLine($"{Name} is examining the animals.");
    }
}

// Class for food inventory
class FoodInventory
{
    public void CheckAvailability()
    {
        Console.WriteLine("Checking food inventory...");
        // Logic to check availability of food
    }
}

// Class for animal inventory
class AnimalInventory
{
    public void CheckAvailability()
    {
        Console.WriteLine("Checking animal inventory...");
        // Logic to check availability of animals
    }
}

// Class for employee inventory
class EmployeeInventory
{
    public void CheckAvailability()
    {
        Console.WriteLine("Checking employee inventory...");
        // Logic to check availability of employees
    }
}

// Class to manage zoo
class Zoo
{
    private List<Enclosure> enclosures;
    private List<IAnimal> animals;
    private List<IEmployee> employees;
    private FoodInventory foodInventory;
    private AnimalInventory animalInventory;
    private EmployeeInventory employeeInventory;

    public Zoo()
    {
        enclosures = new List<Enclosure>();
        animals = new List<IAnimal>();
        employees = new List<IEmployee>();
        foodInventory = new FoodInventory();
        animalInventory = new AnimalInventory();
        employeeInventory = new EmployeeInventory();
    }

    // Methods for managing zoo
    public void AddEnclosure(Enclosure enclosure)
    {
        enclosures.Add(enclosure);
    }

    public void AddAnimal(IAnimal animal)
    {
        animals.Add(animal);
    }

    public void AddEmployee(IEmployee employee)
    {
        employees.Add(employee);
    }

    public void PerformInventory()
    {
        foodInventory.CheckAvailability();
        animalInventory.CheckAvailability();
        employeeInventory.CheckAvailability();
    }

    public List<Enclosure> Enclosures { get { return enclosures; } }
    public List<IAnimal> Animals { get { return animals; } }
    public List<IEmployee> Employees { get { return employees; } }
}

class Program
{
    static void Main(string[] args)
    {
        Zoo zoo = new Zoo();

        zoo.AddEnclosure(new Cage(10));
        zoo.AddEnclosure(new Cage(20));

        zoo.AddAnimal(new Lion { Name = "Simba" });
        zoo.AddAnimal(new Tiger { Name = "Shera" });

        zoo.AddEmployee(new Zookeeper { Name = "John" });
        zoo.AddEmployee(new Veterinarian { Name = "Emily" });

        zoo.PerformInventory();

        Console.WriteLine("\nZoo information: \n");
        Console.WriteLine("Enclosures: ");
        foreach (var enclosure in zoo.Enclosures)
        {
            Console.WriteLine($"Type: {enclosure.Type}, Size: {enclosure.Size}");
        }

        Console.WriteLine("\nAnimals:");
        foreach (var animal in zoo.Animals)
        {
            Console.WriteLine($"Type: {animal.GetType().Name}, Name: {animal.Name}");
        }

        Console.WriteLine("\nEmployees:");
        foreach (var employee in zoo.Employees)
        {
            Console.WriteLine($"Type: {employee.GetType().Name}, Name: {employee.Name}");
        }
    }
}
